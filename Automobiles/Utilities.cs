﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Automobiles.Models;
using System.Configuration;

namespace Automobiles
{
    public class Utilities
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["tocCatalogConnectionString"].ConnectionString;
       
        public static T DeserializeGeneric<T>(string serializedObject) where T : new()
        {
            try
            {
                if (serializedObject == null)
                {
                    return default(T);
                }
                if (serializedObject.Length == 0)
                {
                    if (default(T) != null)
                    {
                        return default(T);
                    }
                    return Activator.CreateInstance<T>();
                }
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (StringReader reader = new StringReader(serializedObject))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string SerializeObject<T>(T serializedObject) where T : new()
        {
            try
            {
                if (serializedObject == null)
                {
                    return string.Empty;
                }
                else
                {
                    MemoryStream ms = new MemoryStream();
                    System.Xml.Serialization.XmlSerializerNamespaces xns = new System.Xml.Serialization.XmlSerializerNamespaces();
                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(serializedObject.GetType());
                    xns.Add(string.Empty, string.Empty);
                    x.Serialize(ms, serializedObject, xns);
                    ms.Position = 0;
                    var reader = new StreamReader(ms);
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal static CarMenu GetCarMenu(string strInfo)
        {
            CarMenu carMenu = DeserializeGeneric<CarMenu>(strInfo);
            return carMenu;
        }
        internal static CarFilters GetCarFilters(string strInfo)
        {
            CarFilters carFilter = DeserializeGeneric<CarFilters>(strInfo);
            return carFilter;
        }
        internal static CarColors GetCarColors(string strInfo)
        {
            CarColors carColors= DeserializeGeneric<CarColors>(strInfo);
            return carColors;
        }
        internal static CarFeatures GetCarFeatures(string strInfo)
        {
            CarFeatures carFeature = DeserializeGeneric<CarFeatures>(strInfo);
            return carFeature;
        }
        internal static CarImages GetCarImages(string strInfo)
        {
            CarImages carImages= DeserializeGeneric<CarImages>(strInfo);
            return carImages;
        }
        internal static CarBroucher GetCarBroucher(string strInfo)
        {
            CarBroucher carBroucher = DeserializeGeneric<CarBroucher>(strInfo);
            return carBroucher;
        }
        internal static CarVariant GetCarVariant(string strInfo)
        {
            CarVariant carVariant= DeserializeGeneric<CarVariant>(strInfo);
            return carVariant;
        }
        internal static CarGallery GetCarGallery(string strInfo)
        {
            CarGallery carGallery = DeserializeGeneric<CarGallery>(strInfo);
            return carGallery;
        }
        internal static CompareGallery GetCompareGallery(string strInfo)
        {
            CompareGallery carCmpGallery = DeserializeGeneric<CompareGallery>(strInfo);
            return carCmpGallery;
        }
        internal static VideoSlideshow GetVideoSlideshow(string strInfo)
        {
            VideoSlideshow videoData = DeserializeGeneric<VideoSlideshow>(strInfo);
            return videoData;
        }
        internal static Overview GetOverviewData(string strInfo)
        {
            Overview overvwData = DeserializeGeneric<Overview>(strInfo);
            return overvwData;
        }
        internal static Audio GetAudioData(string strInfo)
        {
            Audio audioData = DeserializeGeneric<Audio>(strInfo);
            return audioData;
        }
    }
}
