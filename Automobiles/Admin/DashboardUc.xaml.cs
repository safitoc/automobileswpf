﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobiles.Admin
{
    /// <summary>
    /// Interaction logic for DashboardUc.xaml
    /// </summary>
    public partial class DashboardUc : UserControl
    {
        public DashboardUc()
        {
            InitializeComponent();
        }

        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventLogoutSettings;
        public event EventHandler EventOpenSetAutoSync;
        #endregion
        private void btnRestart_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (MessageBox.Show("Are you sure want to restart the system.", "Automobiles", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/r /f /t 0");
            }
            Logger.Info("Completed");
        }

        private void btnShutdown_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (MessageBox.Show("Are you sure want to shutdown the system.", "Automobiles", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("shutdown", "/s /f /t 0");
            }
            Logger.Info("Completed");
        }

        private void btnAutosync_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Storyboard CloseSB1 = TryFindResource("CloseSB") as Storyboard;
            CloseSB1.Completed += new EventHandler(CloseSB1_Completed);
            CloseSB1.Begin();
            Logger.Info("Initiated");
        }
        void CloseSB1_Completed(object sender, EventArgs e)
        {
            EventOpenSetAutoSync(this, null);
        }

        private void btnLogout_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Storyboard CloseSB = TryFindResource("CloseSB") as Storyboard;
            CloseSB.Completed += new EventHandler(CloseSB_Completed);
            CloseSB.Begin();
            Logger.Info("Initiated");
        }
        void CloseSB_Completed(object sender, EventArgs e)
        {
            EventLogoutSettings(this, null);
        }
    }
}
