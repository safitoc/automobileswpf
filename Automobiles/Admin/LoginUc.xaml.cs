﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Automobiles.Admin
{
    /// <summary>
    /// Interaction logic for LoginUc.xaml
    /// </summary>
    public partial class LoginUc : UserControl
    {
        #region variables
        public event EventHandler EventCloseAdminLogin;
        public event EventHandler EventAdminLogin;
        #endregion
        public LoginUc()
        {
            InitializeComponent();
        }
        private void btnCancelLogin_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
            Storyboard CloseSB = TryFindResource("CloseSB") as Storyboard;
            CloseSB.Completed +=  new EventHandler(CloseSB_Completed);
            CloseSB.Begin();
        }

        void CloseSB_Completed(object sender, EventArgs e)
        {
            EventCloseAdminLogin(this, null);
        }

        private void btnAdminLogin_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
            Storyboard CloseSB1 = TryFindResource("CloseSB") as Storyboard;
            CloseSB1.Completed += new EventHandler(CloseSB1_Completed);
            CloseSB1.Begin();
        }
        void CloseSB1_Completed(object sender, EventArgs e)
        {
            EventAdminLogin(this, null);
        }
        private void ValidateLogin()
        {
            //Logger.Info("Initiated");
            //CloseTabTip();
            //if (!string.IsNullOrEmpty(txtName.Text) && !string.IsNullOrEmpty(txtPassword.Password))
            //{
            //    if (Apps.PingCheck())
            //    {
            //        if (Apps.ValidateAdmin(txtName.Text, txtPassword.Password))
            //        {
            //            SaveCredentials(txtName.Text.Trim(), txtPassword.Password.Trim());
            //            EventLoginAdmin(this, null);
            //        }
            //        else
            //        {
            //            EventWarningAlert("Invalid username and password.", null);
            //            //MessageBox.Show("Invalid username and password.", "Apollo", MessageBoxButton.OK);
            //            ClearFields();
            //        }
            //    }
            //    else if (txtName.Text.Trim().Equals(Utilities.UserName) && txtPassword.Password.Trim().Equals(Utilities.Password))
            //    {
            //        EventLoginAdmin(this, null);
            //    }
            //    else
            //    {
            //        ClearFields();
            //        EventWarningAlert("Invalid username and password.", null);
            //        //MessageBox.Show("Invalid username and password.", "Apollo", MessageBoxButton.OK);
            //    }
            //}
            //else
            //{
            //    ClearFields();
            //    EventWarningAlert("Username and Password should not be empty.", null);
            //    //MessageBox.Show("Username and Password should not be empty.", "Apollo", MessageBoxButton.OK);
            //}
            //Logger.Info("Completed");
        }

        private void txtLoginID_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
        }
        private void txtLoginID_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();

        }
        private void txtPassword_TouchDown(object sender, TouchEventArgs e)
        {
            Apps.KillTabTip();
        }
        private void txtPassword_TouchLeave(object sender, TouchEventArgs e)
        {
            Apps.OpenVirtualKeyboard();
        }
    }
}
