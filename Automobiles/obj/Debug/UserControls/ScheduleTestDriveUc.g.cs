﻿#pragma checksum "..\..\..\UserControls\ScheduleTestDriveUc.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1B796EACBCF898BEC11AE6D1DEFDF66F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Automobiles.UserControls {
    
    
    /// <summary>
    /// ScheduleTestDriveUc
    /// </summary>
    public partial class ScheduleTestDriveUc : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.Card MainLayout;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridContext;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NameTextBox;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox PhoneTextBox;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EmailTextBox;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker FutureDatePicker;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSumbit;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Automobiles;component/usercontrols/scheduletestdriveuc.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MainLayout = ((MaterialDesignThemes.Wpf.Card)(target));
            return;
            case 2:
            this.gridContext = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.btnClose.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnClose_TouchDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.NameTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 63 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.NameTextBox.TouchEnter += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchEnter_TouchEnter);
            
            #line default
            #line hidden
            
            #line 64 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.NameTextBox.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchLeave_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 5:
            this.PhoneTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 69 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.PhoneTextBox.TouchEnter += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchEnter_TouchEnter);
            
            #line default
            #line hidden
            
            #line 70 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.PhoneTextBox.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchLeave_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 6:
            this.EmailTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 75 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.EmailTextBox.TouchEnter += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchEnter_TouchEnter);
            
            #line default
            #line hidden
            
            #line 76 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.EmailTextBox.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchLeave_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 83 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            ((System.Windows.Controls.TextBox)(target)).TouchEnter += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchEnter_TouchEnter);
            
            #line default
            #line hidden
            
            #line 84 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            ((System.Windows.Controls.TextBox)(target)).TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.TestDriveTouchLeave_TouchLeave);
            
            #line default
            #line hidden
            return;
            case 8:
            this.FutureDatePicker = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 9:
            this.btnSumbit = ((System.Windows.Controls.Button)(target));
            
            #line 93 "..\..\..\UserControls\ScheduleTestDriveUc.xaml"
            this.btnSumbit.TouchDown += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.btnSumbit_TouchDown);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

