﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for GalleryUc.xaml
    /// </summary>
    public partial class GalleryUc : UserControl
    {
        public List<GalleryItem> gItem { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventOpenExteriorImages;
        public event EventHandler EventOpenInteriorImages;
        public event EventHandler EventCloseImageControl;
        public GalleryUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            if (gItem != null)
            {
                string type = "exterior";
                List<GalleryItem> lstExteriorImages = gItem.Where(c => c.Type.Equals(type)).ToList();
                EventOpenExteriorImages(lstExteriorImages, null);
            }
            Logger.Info("Completed");
        }
        private void btnExterior_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseImageControl(this, null);
            Grid.SetColumn(RectHighlight, 0);
            string type = "exterior";
            List<GalleryItem> lstExteriorImages = gItem.Where(c => c.Type.Equals(type)).ToList();
            EventOpenExteriorImages(lstExteriorImages, null);
            Logger.Info("Completed");
        }

        private void btnInterior_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            EventCloseImageControl(this, null);
            Grid.SetColumn(RectHighlight, 1);
            string type = "interior";
            List<GalleryItem> lstInteriorImages = gItem.Where(c => c.Type.Equals(type)).ToList();
            EventOpenInteriorImages(lstInteriorImages, null);
            Logger.Info("Completed");
        }
    }
}
