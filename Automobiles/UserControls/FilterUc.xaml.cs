﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using NLog;
using System.Configuration;
using System.IO;
using System.Windows.Media.Animation;
using TOCCatalogDB.Models;
using Microsoft.Expression.Drawing;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for FilterUc.xaml
    /// </summary>
    public partial class FilterUc : UserControl
    {
        public CarFilters carFilter { get; set; }
        
        public List<FilterTitle> prodFilter { get; set; }
        FilterTitle filter = new FilterTitle();

        //public string selectedCarId { get; set; }
        public bool isFilterOpen { get; set; }
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public event EventHandler EventColorControl;

        public event EventHandler EventOpenCarImagesUc;
        public event EventHandler EventOpenGalleryUc;
        public event EventHandler EventOpenVariantsUc;
        public event EventHandler EventOpenBroucherUc;
        public event EventHandler EventLoadCarMenu;
        public FilterUc()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            try
            {

                filterContext.ItemsSource = prodFilter.ToList();
                
                //filterContext.ItemsSource = carFilter.FilterItem.Where(c => c.CarID.Equals(selectedCarId)).ToList();
                
                isFilterOpen = true;
                OpenMenuStoryboard();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
            Logger.Info("Completed");
        }
        
        private void btnFilterItem_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            if (btn != null && btn.DataContext != null)
            {

                filter = btn.DataContext as FilterTitle;
                if (filter != null)
                {
                    switch (filter.DataType)
                    {
                        case TOCCatalogDB.Enums.FilterDataType.Checkbox:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.Color:
                            CloseMenuStoryboard();
                            if (filter.LstProduct != null && filter.LstProduct.Count > 0)
                            {
                                EventColorControl(this, null);
                            }
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.Date:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.DropDown:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.List:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.MultiCheckbox:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.MultiChoice:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.Numeric:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.RadioButton:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.TextBox:
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.UploadDocument:
                            CloseMenuStoryboard();
                            
                            break;
                        case TOCCatalogDB.Enums.FilterDataType.UploadImage:
                            break;
                        default:
                            break;
                    }
                }
                //FilterDetail item = btn.DataContext as FilterDetail;
                //if (item != null)
                //{
                //    switch (item.Name)
                //    {
                //        case "Overview":
                //            CloseMenuStoryboard();
                //            EventOpenCarImagesUc(item, null);
                //            break;
                //        case "Gallery":
                //            CloseMenuStoryboard();
                //            EventOpenGalleryUc(item, null);
                //            break;
                //        case "Variants":
                //            CloseMenuStoryboard();
                //            EventOpenVariantsUc(item, null);
                //            break;
                //        case "Brochures":
                //            CloseMenuStoryboard();
                //            EventOpenBroucherUc(item, null);
                //            break;
                //        case "ViewCars":
                //            CloseMenuStoryboard();
                //            EventLoadCarMenu(this, null);
                //            break;
                //        default: break;
                //    }
                //}
            }
            Logger.Info("Completed");
        }
        private void btnMap_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (isFilterOpen) //Storyboard to run if menu is open
            {
                Storyboard RotateReverseCategoryIcon = TryFindResource("RotateReverseCategoryIcon") as Storyboard;
                RotateReverseCategoryIcon.Begin();
                CloseMenuStoryboard();
            }
            else if (!isFilterOpen) //Storyboard to run if menu is closed
            {
                Storyboard RotateCategoryIcon = TryFindResource("RotateCategoryIcon") as Storyboard;
                RotateCategoryIcon.Begin();
                OpenMenuStoryboard();
            }
        }

        private void OpenMenuStoryboard()
        {
            Storyboard Sb_OpenFilter = TryFindResource("Sb_OpenFilter") as Storyboard;
            Sb_OpenFilter.Completed += new EventHandler(Sb_OpenFilter_Completed);
            Sb_OpenFilter.Begin();
        }

        private void CloseMenuStoryboard()
        {
            Storyboard Sb_CloseFilter = TryFindResource("Sb_CloseFilter") as Storyboard;
            Sb_CloseFilter.Completed += new EventHandler(Sb_CloseFilter_Completed);
            Sb_CloseFilter.Begin();
        }
        void Sb_OpenFilter_Completed(object sender, EventArgs e)
        {
            isFilterOpen = true;
        }
        void Sb_CloseFilter_Completed(object sender, EventArgs e)
        {
            isFilterOpen = false;
        }
    }
}
