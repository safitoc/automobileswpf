﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.Models;
using System.Configuration;
using System.IO;
using NLog;
using TOCCatalogDB.Models;

namespace Automobiles.UserControls
{
    /// <summary>
    /// Interaction logic for ColorsUc.xaml
    /// </summary>
    public partial class ColorsUc : UserControl
    {
        #region variables
        private Logger Logger = LogManager.GetCurrentClassLogger();
        public List<Product> lstProductsColorWise { get; set; }
        public event EventHandler EventShowSelectedColorCar;

        public event EventHandler EventShowSelectedCar;
        CarColors carColor;
        public string selectedCarId { get; set; }
        #endregion
        public ColorsUc()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ICtrlColor.ItemsSource = lstProductsColorWise;

            //string carColorFolder = ConfigurationManager.AppSettings["CarColorsPath"];
            //string path = System.IO.Path.Combine(Apps.strPath, carColorFolder);
            //if (File.Exists(path))
            //{
            //    string strInfo = System.IO.File.ReadAllText(path);
            //    carColor = Utilities.GetCarColors(strInfo);
            //    if (carColor != null)
            //    {
            //        ICtrlColor.ItemsSource = carColor.ColorItem.Where(c => c.CarID.Equals(selectedCarId));
            //    }
            //}
        }

        private void btnCarColorItem_TouchDown(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                Product selectedProduct = btn.DataContext as Product;
                //ColorItem coloritem = selectedCar.DataContext as ColorItem;
                if (selectedProduct != null && EventShowSelectedColorCar != null)
                {
                    EventShowSelectedColorCar(selectedProduct, null);
                }
            }
        }
    }
}
