﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "CompareGalleryItem")]
    public class CompareGalleryItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
    }

    [XmlRoot(ElementName = "CompareGallery")]
    public class CompareGallery
    {
        [XmlElement(ElementName = "CompareGalleryItem")]
        public List<CompareGalleryItem> CompareGalleryItem { get; set; }
    }
}
