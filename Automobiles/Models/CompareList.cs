﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automobiles.Models;

namespace Automobiles.Models
{
    public class CompareList : INotifyPropertyChanged
    {
        public CompareList()
        {
            LstCompareProducts = new ObservableCollection<VariantItem>();
        }

        private ObservableCollection<VariantItem> lstCompareProducts;
        public ObservableCollection<VariantItem> LstCompareProducts
        {
            get { return lstCompareProducts; }
            set
            {
                if (value is ObservableCollection<VariantItem>)
                {
                    foreach (VariantItem item in value)
                    {
                        if (!lstCompareProducts.Contains(item))
                            lstCompareProducts.Add(item);
                    }

                    if (lstCompareProducts == null)
                        lstCompareProducts = value;
                }
                OnPropertyChanged("LstCompareProducts");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(e));
            }
        }
    }
}
