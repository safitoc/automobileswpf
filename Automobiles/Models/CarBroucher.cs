﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "BroucherItem")]
    public class BroucherItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "ID")]
        public string ID { get; set; }
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
        [XmlElement(ElementName = "PopUpLocation")]
        public string PopUpLocation { get; set; }
    }

    [XmlRoot(ElementName = "CarBroucher")]
    public class CarBroucher
    {
        [XmlElement(ElementName = "BroucherItem")]
        public List<BroucherItem> BroucherItem { get; set; }
    }
}
