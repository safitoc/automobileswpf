﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "VideoItem")]
    public class VideoItem
    {
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "VideoLocation")]
        public string VideoLocation { get; set; }
    }

    [XmlRoot(ElementName = "VideoSlideshow")]
    public class VideoSlideshow
    {
        [XmlElement(ElementName = "VideoItem")]
        public List<VideoItem> VideoItem { get; set; }
    }

}
