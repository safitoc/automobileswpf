﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName="VariantItem")]
	public class VariantItem {
		[XmlElement(ElementName="SNo")]
		public string SNo { get; set; }
		[XmlElement(ElementName="Name")]
		public string Name { get; set; }
		[XmlElement(ElementName="Version")]
		public string Version { get; set; }
		[XmlElement(ElementName="IsSelected")]
		public string IsSelected { get; set; }
		[XmlElement(ElementName="CarID")]
		public string CarID { get; set; }
		[XmlElement(ElementName="ImageLocation")]
		public string ImageLocation { get; set; }
		[XmlElement(ElementName="Description")]
		public string Description { get; set; }
		[XmlElement(ElementName="Length")]
		public string Length { get; set; }
		[XmlElement(ElementName="Width")]
		public string Width { get; set; }
		[XmlElement(ElementName="Height")]
		public string Height { get; set; }
		[XmlElement(ElementName="Wheelbase")]
		public string Wheelbase { get; set; }
		[XmlElement(ElementName="GroundClearance")]
		public string GroundClearance { get; set; }
		[XmlElement(ElementName="KerbWeight")]
		public string KerbWeight { get; set; }
		[XmlElement(ElementName="Doors")]
		public string Doors { get; set; }
		[XmlElement(ElementName="SeatingCapacity")]
		public string SeatingCapacity { get; set; }
		[XmlElement(ElementName="NoOfSeatingRows")]
		public string NoOfSeatingRows { get; set; }
		[XmlElement(ElementName="Bootspace")]
		public string Bootspace { get; set; }
		[XmlElement(ElementName="FuelTankCapacity")]
		public string FuelTankCapacity { get; set; }
		[XmlElement(ElementName="EngineType")]
		public string EngineType { get; set; }
		[XmlElement(ElementName="Cylinder")]
		public string Cylinder { get; set; }
		[XmlElement(ElementName="Displacement")]
		public string Displacement { get; set; }
		[XmlElement(ElementName="Mileage")]
		public string Mileage { get; set; }
		[XmlElement(ElementName="Turbocharger")]
		public string Turbocharger { get; set; }
		[XmlElement(ElementName="Drivetrain")]
		public string Drivetrain { get; set; }
		[XmlElement(ElementName="DualClutch")]
		public string DualClutch { get; set; }
		[XmlElement(ElementName="SportMode")]
		public string SportMode { get; set; }
		[XmlElement(ElementName="Bore")]
		public string Bore { get; set; }
		[XmlElement(ElementName="AlternateFuel")]
		public string AlternateFuel { get; set; }
		[XmlElement(ElementName="Fuel")]
		public string Fuel { get; set; }
		[XmlElement(ElementName="MaxPower")]
		public string MaxPower { get; set; }
		[XmlElement(ElementName="MaxTorque")]
		public string MaxTorque { get; set; }
		[XmlElement(ElementName="TurbochargerType")]
		public string TurbochargerType { get; set; }
		[XmlElement(ElementName="NoOfGears")]
		public string NoOfGears { get; set; }
		[XmlElement(ElementName="TransmissionType")]
		public string TransmissionType { get; set; }
		[XmlElement(ElementName="ManualShiftingOfAutomatic")]
		public string ManualShiftingOfAutomatic { get; set; }
		[XmlElement(ElementName="DrivingModes")]
		public string DrivingModes { get; set; }
		[XmlElement(ElementName="EngineStartStopFunction")]
		public string EngineStartStopFunction { get; set; }
		[XmlElement(ElementName="SuspensionFront")]
		public string SuspensionFront { get; set; }
		[XmlElement(ElementName="SuspensionRear")]
		public string SuspensionRear { get; set; }
		[XmlElement(ElementName="FrontBrakeType")]
		public string FrontBrakeType { get; set; }
		[XmlElement(ElementName="RearBrakeType")]
		public string RearBrakeType { get; set; }
		[XmlElement(ElementName="SteeringType")]
		public string SteeringType { get; set; }
		[XmlElement(ElementName="Wheels")]
		public string Wheels { get; set; }
		[XmlElement(ElementName="SpareWheel")]
		public string SpareWheel { get; set; }
		[XmlElement(ElementName="FrontTyres")]
		public string FrontTyres { get; set; }
		[XmlElement(ElementName="RearTyres")]
		public string RearTyres { get; set; }
		[XmlElement(ElementName="Price")]
		public string Price { get; set; }
	}

	[XmlRoot(ElementName="CarVariant")]
	public class CarVariant {
		[XmlElement(ElementName="VariantItem")]
		public List<VariantItem> VariantItem { get; set; }
	}    
}
