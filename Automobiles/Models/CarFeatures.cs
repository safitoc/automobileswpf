﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automobiles.Models
{
    [XmlRoot(ElementName = "FeaturesItem")]
    public class FeaturesItem
    {
        [XmlElement(ElementName = "SNo")]
        public string SNo { get; set; }
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Version")]
        public string Version { get; set; }
        [XmlElement(ElementName = "IsSelected")]
        public string IsSelected { get; set; }
        [XmlElement(ElementName = "CarID")]
        public string CarID { get; set; }
        [XmlElement(ElementName = "ImageLocation")]
        public string ImageLocation { get; set; }
        [XmlElement(ElementName = "Airbags")]
        public string Airbags { get; set; }
        [XmlElement(ElementName = "DualStageAirbags")]
        public string DualStageAirbags { get; set; }
        [XmlElement(ElementName = "MiddleRearThreePointSeatbelt")]
        public string MiddleRearThreePointSeatbelt { get; set; }
        [XmlElement(ElementName = "MiddleRearHeadRest")]
        public string MiddleRearHeadRest { get; set; }
        [XmlElement(ElementName = "TPMS")]
        public string TPMS { get; set; }
        [XmlElement(ElementName = "ChildSeatAnchorPoint")]
        public string ChildSeatAnchorPoint { get; set; }
        [XmlElement(ElementName = "SeatBeltWarning")]
        public string SeatBeltWarning { get; set; }
        [XmlElement(ElementName = "ABS")]
        public string ABS { get; set; }
        [XmlElement(ElementName = "EBD")]
        public string EBD { get; set; }
        [XmlElement(ElementName = "BA")]
        public string BA { get; set; }
        [XmlElement(ElementName = "ESP")]
        public string ESP { get; set; }
        [XmlElement(ElementName = "FourWheelDrive")]
        public string FourWheelDrive { get; set; }
        [XmlElement(ElementName = "HillHoldControl")]
        public string HillHoldControl { get; set; }
        [XmlElement(ElementName = "TCS")]
        public string TCS { get; set; }
        [XmlElement(ElementName = "RideHeightAdjustment")]
        public string RideHeightAdjustment { get; set; }
        [XmlElement(ElementName = "HillDescentControl")]
        public string HillDescentControl { get; set; }
        [XmlElement(ElementName = "LSD")]
        public string LSD { get; set; }
        [XmlElement(ElementName = "DifferentialLock")]
        public string DifferentialLock { get; set; }
        [XmlElement(ElementName = "EngineImmobilizer")]
        public string EngineImmobilizer { get; set; }
        [XmlElement(ElementName = "CentralLocking")]
        public string CentralLocking { get; set; }
        [XmlElement(ElementName = "SpeedSensingDoorLock")]
        public string SpeedSensingDoorLock { get; set; }
        [XmlElement(ElementName = "ChildSafetyLock")]
        public string ChildSafetyLock { get; set; }
        [XmlElement(ElementName = "AirConditioner")]
        public string AirConditioner { get; set; }
        [XmlElement(ElementName = "RearAC")]
        public string RearAC { get; set; }
        [XmlElement(ElementName = "PowerOutlet")]
        public string PowerOutlet { get; set; }
        [XmlElement(ElementName = "SteeringAdjustment")]
        public string SteeringAdjustment { get; set; }
        [XmlElement(ElementName = "KeylessStart")]
        public string KeylessStart { get; set; }
        [XmlElement(ElementName = "IgnitionKeyOffReminder")]
        public string IgnitionKeyOffReminder { get; set; }
        [XmlElement(ElementName = "CruiseConrol")]
        public string CruiseConrol { get; set; }
        [XmlElement(ElementName = "ParkingSensors")]
        public string ParkingSensors { get; set; }
        [XmlElement(ElementName = "ParkingAssist")]
        public string ParkingAssist { get; set; }
        [XmlElement(ElementName = "AntiGlareMirrors")]
        public string AntiGlareMirrors { get; set; }
        [XmlElement(ElementName = "VanityMirrorsOnSunVisors")]
        public string VanityMirrorsOnSunVisors { get; set; }
        [XmlElement(ElementName = "Heater")]
        public string Heater { get; set; }
        [XmlElement(ElementName = "CabinBootAccess")]
        public string CabinBootAccess { get; set; }
        [XmlElement(ElementName = "SeatUpholstery")]
        public string SeatUpholstery { get; set; }
        [XmlElement(ElementName = "LeatherWrappedSteeringWheel")]
        public string LeatherWrappedSteeringWheel { get; set; }
        [XmlElement(ElementName = "LeatherWrappedGearKnob")]
        public string LeatherWrappedGearKnob { get; set; }
        [XmlElement(ElementName = "DriverSeatAdjustment")]
        public string DriverSeatAdjustment { get; set; }
        [XmlElement(ElementName = "HeadRests")]
        public string HeadRests { get; set; }
        [XmlElement(ElementName = "FrontPassengerSeatAdjustment")]
        public string FrontPassengerSeatAdjustment { get; set; }
        [XmlElement(ElementName = "LumbarSupport")]
        public string LumbarSupport { get; set; }
        [XmlElement(ElementName = "AdjustableLumbarSupport")]
        public string AdjustableLumbarSupport { get; set; }
        [XmlElement(ElementName = "DriverArmest")]
        public string DriverArmest { get; set; }
        [XmlElement(ElementName = "RearPassengerSeats")]
        public string RearPassengerSeats { get; set; }
        [XmlElement(ElementName = "RearPassengerAdjustableSeats")]
        public string RearPassengerAdjustableSeats { get; set; }
        [XmlElement(ElementName = "RowSeats")]
        public string RowSeats { get; set; }
        [XmlElement(ElementName = "RowSeatAdjustment")]
        public string RowSeatAdjustment { get; set; }
        [XmlElement(ElementName = "VentilatedSeats")]
        public string VentilatedSeats { get; set; }
        [XmlElement(ElementName = "VentilatedSeatType")]
        public string VentilatedSeatType { get; set; }
        [XmlElement(ElementName = "RearArmrest")]
        public string RearArmrest { get; set; }
        [XmlElement(ElementName = "FoldeingRearSeat")]
        public string FoldeingRearSeat { get; set; }
        [XmlElement(ElementName = "SplitRearSeat")]
        public string SplitRearSeat { get; set; }
        [XmlElement(ElementName = "SplitThirdRowSeat")]
        public string SplitThirdRowSeat { get; set; }
        [XmlElement(ElementName = "FrontSeatPockets")]
        public string FrontSeatPockets { get; set; }
        [XmlElement(ElementName = "AdjustableHeadRests")]
        public string AdjustableHeadRests { get; set; }
        [XmlElement(ElementName = "ElectricallyAdjustableHeadrest")]
        public string ElectricallyAdjustableHeadrest { get; set; }
        [XmlElement(ElementName = "CupHolders")]
        public string CupHolders { get; set; }
        [XmlElement(ElementName = "DriverArmrestStorage")]
        public string DriverArmrestStorage { get; set; }
        [XmlElement(ElementName = "CooledGloveBox")]
        public string CooledGloveBox { get; set; }
        [XmlElement(ElementName = "SunglassHolder")]
        public string SunglassHolder { get; set; }
        [XmlElement(ElementName = "ThirdRowCupHolders")]
        public string ThirdRowCupHolders { get; set; }
        [XmlElement(ElementName = "OneTouchDown")]
        public string OneTouchDown { get; set; }
        [XmlElement(ElementName = "OneTouchUp")]
        public string OneTouchUp { get; set; }
        [XmlElement(ElementName = "PowerWindows")]
        public string PowerWindows { get; set; }
        [XmlElement(ElementName = "ORVM")]
        public string ORVM { get; set; }
        [XmlElement(ElementName = "AdjustableORVM")]
        public string AdjustableORVM { get; set; }
        [XmlElement(ElementName = "TurnIndicatorsOnORVM")]
        public string TurnIndicatorsOnORVM { get; set; }
        [XmlElement(ElementName = "RearDefogger")]
        public string RearDefogger { get; set; }
        [XmlElement(ElementName = "RearWiper")]
        public string RearWiper { get; set; }
        [XmlElement(ElementName = "ExteriorDoorHandles")]
        public string ExteriorDoorHandles { get; set; }
        [XmlElement(ElementName = "InteriorDoorHandles")]
        public string InteriorDoorHandles { get; set; }
        [XmlElement(ElementName = "RainSensingWipers")]
        public string RainSensingWipers { get; set; }
        [XmlElement(ElementName = "DoorPockets")]
        public string DoorPockets { get; set; }
        [XmlElement(ElementName = "DoorBlinds")]
        public string DoorBlinds { get; set; }
        [XmlElement(ElementName = "RearWindowBlind")]
        public string RearWindowBlind { get; set; }
        [XmlElement(ElementName = "BootLidOpener")]
        public string BootLidOpener { get; set; }
        [XmlElement(ElementName = "Sunroof")]
        public string Sunroof { get; set; }
        [XmlElement(ElementName = "RoofRails")]
        public string RoofRails { get; set; }
        [XmlElement(ElementName = "RoofMountedAntenan")]
        public string RoofMountedAntenan { get; set; }
        [XmlElement(ElementName = "BodyColouredBumpers")]
        public string BodyColouredBumpers { get; set; }
        [XmlElement(ElementName = "ChromeFinishExhaustPipe")]
        public string ChromeFinishExhaustPipe { get; set; }
        [XmlElement(ElementName = "BodyKit")]
        public string BodyKit { get; set; }
        [XmlElement(ElementName = "RubStrips")]
        public string RubStrips { get; set; }
        [XmlElement(ElementName = "CorneringHeadlights")]
        public string CorneringHeadlights { get; set; }
        [XmlElement(ElementName = "Headlamps")]
        public string Headlamps { get; set; }
        [XmlElement(ElementName = "AutomaticHeadLamps")]
        public string AutomaticHeadLamps { get; set; }
        [XmlElement(ElementName = "FollowMeHomeHeadlamps")]
        public string FollowMeHomeHeadlamps { get; set; }
        [XmlElement(ElementName = "DaytimeRunningLights")]
        public string DaytimeRunningLights { get; set; }
        [XmlElement(ElementName = "FogLamps")]
        public string FogLamps { get; set; }
        [XmlElement(ElementName = "TailLamps")]
        public string TailLamps { get; set; }
        [XmlElement(ElementName = "CabinLamps")]
        public string CabinLamps { get; set; }
        [XmlElement(ElementName = "HeadlightHeightAdjuster")]
        public string HeadlightHeightAdjuster { get; set; }
        [XmlElement(ElementName = "GloveBoxLamp")]
        public string GloveBoxLamp { get; set; }
        [XmlElement(ElementName = "LightsOnVanityMirrors")]
        public string LightsOnVanityMirrors { get; set; }
        [XmlElement(ElementName = "RearReadingLamp")]
        public string RearReadingLamp { get; set; }
        [XmlElement(ElementName = "InstrumentCluster")]
        public string InstrumentCluster { get; set; }
        [XmlElement(ElementName = "TripMeter")]
        public string TripMeter { get; set; }
        [XmlElement(ElementName = "AverageFuelConsumption")]
        public string AverageFuelConsumption { get; set; }
        [XmlElement(ElementName = "AverageSpeed")]
        public string AverageSpeed { get; set; }
        [XmlElement(ElementName = "DistanceToEmpty")]
        public string DistanceToEmpty { get; set; }
        [XmlElement(ElementName = "Clock")]
        public string Clock { get; set; }
        [XmlElement(ElementName = "LowFuelLevelWarning")]
        public string LowFuelLevelWarning { get; set; }
        [XmlElement(ElementName = "DoorAjarWarning")]
        public string DoorAjarWarning { get; set; }
        [XmlElement(ElementName = "AdjustableClusterBrightness")]
        public string AdjustableClusterBrightness { get; set; }
        [XmlElement(ElementName = "GearIndicator")]
        public string GearIndicator { get; set; }
        [XmlElement(ElementName = "ShiftIndicator")]
        public string ShiftIndicator { get; set; }
        [XmlElement(ElementName = "HUD")]
        public string HUD { get; set; }
        [XmlElement(ElementName = "Tachometer")]
        public string Tachometer { get; set; }
        [XmlElement(ElementName = "InstantaneousConsumption")]
        public string InstantaneousConsumption { get; set; }
        [XmlElement(ElementName = "IntegratedMusicSystem")]
        public string IntegratedMusicSystem { get; set; }
        [XmlElement(ElementName = "HeadUnitSize")]
        public string HeadUnitSize { get; set; }
        [XmlElement(ElementName = "Display")]
        public string Display { get; set; }
        [XmlElement(ElementName = "DisplayScreenForRearPassengers")]
        public string DisplayScreenForRearPassengers { get; set; }
        [XmlElement(ElementName = "GPS")]
        public string GPS { get; set; }
        [XmlElement(ElementName = "Speakers")]
        public string Speakers { get; set; }
        [XmlElement(ElementName = "USB")]
        public string USB { get; set; }
        [XmlElement(ElementName = "Aux")]
        public string Aux { get; set; }
        [XmlElement(ElementName = "Bluetooth")]
        public string Bluetooth { get; set; }
        [XmlElement(ElementName = "MP3")]
        public string MP3 { get; set; }
        [XmlElement(ElementName = "CDPlayer")]
        public string CDPlayer { get; set; }
        [XmlElement(ElementName = "DVD")]
        public string DVD { get; set; }
        [XmlElement(ElementName = "AMFM")]
        public string AMFM { get; set; }
        [XmlElement(ElementName = "IpodCompatibility")]
        public string IpodCompatibility { get; set; }
        [XmlElement(ElementName = "InteranHardDrive")]
        public string InteranHardDrive { get; set; }
        [XmlElement(ElementName = "SteeringMountedControl")]
        public string SteeringMountedControl { get; set; }
        [XmlElement(ElementName = "VoiceCommand")]
        public string VoiceCommand { get; set; }
        [XmlElement(ElementName = "Warranty")]
        public string Warranty { get; set; }
    }

    [XmlRoot(ElementName = "CarFeatures")]
    public class CarFeatures
    {
        [XmlElement(ElementName = "FeaturesItem")]
        public List<FeaturesItem> FeaturesItem { get; set; }
    }
}
