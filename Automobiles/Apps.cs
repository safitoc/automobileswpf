﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automobiles.Models;
using System.Windows.Controls;
using System.Collections;
using NLog;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;
using System.IO;

namespace Automobiles
{
    public class Apps
    {
        public static TOCCatalogDB.DBFolder.TOCCatalogDbContext tocCatalogContext;

        public static string strPath = AppDomain.CurrentDomain.BaseDirectory;
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        public static List<VariantItem> lstSelectedVariant { get; set; }

        public static void KillTabTip()
        {
            //TabTip
            try
            {
                if (Process.GetProcessesByName("TabTip").Count() > 0)
                {
                    Process fetchProcess = Process.GetProcessesByName("TabTip").FirstOrDefault();
                    if (fetchProcess != null)
                    {
                        fetchProcess.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }

        public static void OpenVirtualKeyboard()
        {
            try
            {
                Process.Start(@"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }
        }
    }
}
