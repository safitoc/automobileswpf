﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Automobiles.UserControls;
using Automobiles.Models;
using NLog;
using System.Configuration;
using System.IO;
using System.Windows.Media.Animation;
using System.Collections.ObjectModel;
using Automobiles.UserControls.AlertControls;
using System.Windows.Threading;
using TOCCatalogDB;
using TOCCatalogDB.Models;
using Automobiles.Admin;

namespace Automobiles
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Logger Logger = LogManager.GetCurrentClassLogger();

        DispatcherTimer timerImagesChange;
        public CarMenuItem currentSelectedCar { get; set; }
        public TOCCatalogDB.DBFolder.TOCCatalogDbContext DBContext;
        public static int zIndexValue = 999;
        public static int ivRenTrsf;
        public static int ivRenTrsfX, ivRenTrsfY;
        int destinationImageNumber = 0, currentImageNumber = 0;
        bool isOverviewItemDisplayed = true;
        bool IsFloatingBtnSelected = false;
        CarImages carImagesData;
        CarGallery carGalleryData;
        OverviewItem ovItem;
        CarUc car360 = new CarUc();
        Audio audioCtrl = new Audio();
        List<FilterTitle> lstFilterTitle = new List<FilterTitle>();

        public MainWindow()
        {
            InitializeComponent();
            ivRenTrsf = 10;            
        }

        private int SetZIndexValue()    //increment ZIndex value method
        {
            return zIndexValue++;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.Info("Initiated");
            List<Category> categ = Apps.tocCatalogContext.Category.ToList();
            LoadAudioControl();
            #region Manipulation Events
            this.ManipulationStarting -= HomePage_ManipulationStarting;
            this.ManipulationDelta -= HomePage_ManipulationDelta;
            this.ManipulationStarting += HomePage_ManipulationStarting;
            this.ManipulationDelta += HomePage_ManipulationDelta;
            #endregion
            LoadSlideshow();
            Logger.Info("Completed");
        }

        #region ManipulationEvents

        ManipulationModes currentMode = ManipulationModes.All;
        private void HomePage_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
            {
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);
            }

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
        private void HomePage_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            if (element != null)
            {
                MatrixTransform xform = element.RenderTransform as MatrixTransform;
                Matrix matrix = xform.Matrix;
                ManipulationDelta delta = args.DeltaManipulation;
                Point center = args.ManipulationOrigin;
                matrix.Translate(-center.X, -center.Y);
                matrix.Scale(delta.Scale.X, delta.Scale.Y);
                matrix.Translate(center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);
                xform.Matrix = matrix;
            }
            args.Handled = true;
            base.OnManipulationDelta(args);
        }
        #endregion

        #region Slideshow
        private void LoadSlideshow()
        {
            Logger.Info("Initiated");
            Apps.lstSelectedVariant = new List<VariantItem>();
            btnCompare.Visibility = Visibility.Collapsed;
            btnCompare.Content = "0";
            btnFloatingButton.Visibility = Visibility.Collapsed;
            ivRenTrsf = 10;
            zIndexValue = 999;
            GridCarPanel.Children.Clear();
            GridCarDetailsPanel.Children.Clear();
            GridSlideshowPanel.Children.Clear();
            GridFloatingContentPanel.Children.Clear();
            GridFloatingPopUpPanel.Children.Clear();
            GridFloatingPopUpPanel.Children.Clear();
            GridPopUpPanel.Children.Clear();
            
            #region CarGallery
            try
            {
                carGalleryData = new CarGallery();
                string carGalFolder = ConfigurationManager.AppSettings["CarGalleryPath"];
                string carGalpath = System.IO.Path.Combine(Apps.strPath, carGalFolder);
                if (File.Exists(carGalpath))
                {
                    string strInfoGallery = System.IO.File.ReadAllText(carGalpath);
                    carGalleryData = Utilities.GetCarGallery(strInfoGallery);
                }
            }
            catch (Exception ex)
            {
                Logger.Info(ex, ex.Message);
            }
            #endregion

            VideoSlideshowUc videoSlideshow = new VideoSlideshowUc();
            GridSlideshowPanel.Children.Add(videoSlideshow);
            videoSlideshow.EventCloseVideoSlideshow += videoSlideshow_EventCloseVideoSlideshow;
            videoSlideshow.EventOpenAdminLogin += videoSlideshow_EventOpenAdminLogin;
            MEAudio.Pause();
            GridAudioPanel.Visibility = Visibility.Collapsed;

            Logger.Info("Completed");
        }

        void videoSlideshow_EventOpenAdminLogin(object sender, EventArgs e)
        {
            AdminLogin();
        }

        void videoSlideshow_EventCloseVideoSlideshow(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            VideoSlideshowUc videoSlideshow = (VideoSlideshowUc)sender;
            GridSlideshowPanel.Children.Remove(videoSlideshow);
            LoadHome();
            Logger.Info("Completed");
        }

        void slideShow_EventCloseSlideshowUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SlideShowUc slideShow = (SlideShowUc)sender;
            GridSlideshowPanel.Children.Remove(slideShow);
            LoadHome();
            Logger.Info("Completed");
        }
        #endregion

        Category category = new Category();

        private void LoadHome()
        {
            Logger.Info("Initiated");
            btnFloatingButton.Visibility = Visibility.Collapsed;
            btnCompare.Visibility = Visibility.Collapsed;
            CloseCarControls();

            //MEAudio.Source = new Uri(audioCtrl.AudioItem.AudioLocation);
            MEAudio.Play();
            //MEAudio.Position = new TimeSpan(0, 0, 32);
            btnAudioVolume.Visibility = Visibility.Visible;
            GridAudioPanel.Visibility = Visibility.Visible;

            //lstProducts = Apps.tocCatalogContext.Product.Where(d => d.LstLogin.ToList().Any(f => f.UserName.Equals("myfadmin"))).ToList();
                
            category = Apps.tocCatalogContext.Category.ToList().Where(c => c.Name.Equals("Audi")).FirstOrDefault();
            CarMenuUc carMenu = new CarMenuUc();
            GridCarPanel.Background = new SolidColorBrush(Colors.Transparent);
            carMenu.lstProducts = category.LstProduct.ToList();
            GridCarPanel.Children.Add(carMenu);
            carMenu.EventOpenCarUc += carMenu_EventOpenCarUc;

            Logger.Info("Completed");
        }
        void carMenu_EventOpenCarUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            //CarMenuItem selectedCar = sender as CarMenuItem;
            Product selectedProduct = sender as Product;

            CloseCarMenuUC();
            btnCompare.Visibility = Visibility.Visible;
            btnFloatingButton.Visibility = Visibility.Visible;
            //load car control
            CarUc carControl = new CarUc();
            carControl.product = selectedProduct;
            //currentSelectedCar = selectedCar;

            GridCarDetailsPanel.Children.Add(carControl);
            FilterUc filters = new FilterUc();
            filters.prodFilter = category.LstFilter.ToList();
            Panel.SetZIndex(filters, SetZIndexValue());
            GridCarDetailsPanel.Children.Add(filters);
            filters.EventColorControl += filters_EventColorControl;
            
            //List<ImageInformation> lstImages = new List<ImageInformation>();
            //lstImages = selectedProduct.LstImages.ToList();

            //string carImagesFolder = ConfigurationManager.AppSettings["CarImagePath"];
            //string carImagespath = System.IO.Path.Combine(Apps.strPath, carImagesFolder);
            //if (File.Exists(carImagespath))
            //{
            //    string strInfo = System.IO.File.ReadAllText(carImagespath);
            //    carImagesData = Utilities.GetCarImages(strInfo);
            //    CarImageItem carItem = carImagesData.CarImageItem.Where(c => c.CarID.Equals(selectedCar.CarID)).FirstOrDefault();
            //    if (carItem != null)
            //    {
            //        CloseCarMenuUC();
            //        btnCompare.Visibility = Visibility.Visible;
            //        btnFloatingButton.Visibility = Visibility.Visible;
            //        //load car control
            //        CarUc carControl = new CarUc();
            //        carControl.carItem = carItem;
            //        currentSelectedCar = selectedCar;
            //        GridCarDetailsPanel.Children.Add(carControl);

            //        //load filter control
            //        #region FilterControl
            //        FilterUc filter = new FilterUc();
            //        string carFilterFolder = ConfigurationManager.AppSettings["CarFilterPath"];
            //        string path = System.IO.Path.Combine(Apps.strPath, carFilterFolder);
            //        if (File.Exists(path))
            //        {
            //            string strFilterInfo = System.IO.File.ReadAllText(path);
            //            CarFilters carFilters = Utilities.GetCarFilters(strFilterInfo);
            //            if (carFilters != null)
            //            {
            //                filter.carFilter = carFilters;
            //                filter.selectedCarId = selectedCar.CarID;
            //                filter.BringIntoView();
            //                Panel.SetZIndex(filter, SetZIndexValue());
            //                GridCarDetailsPanel.Children.Add(filter);
            //                filter.EventOpenCarImagesUc += filter_EventOpenCarImagesUc;
            //                filter.EventOpenGalleryUc += filter_EventOpenGalleryUc;
            //                filter.EventOpenVariantsUc += filter_EventOpenVariantsUc;
            //                filter.EventOpenBroucherUc += filter_EventOpenBroucherUc;
            //                filter.EventLoadCarMenu += filter_EventLoadCarMenu;
            //            }
            //        }
            //        #endregion
            //        //load overview control
            //        LoadOverview(carItem.CarID);
            //    }
            //}
            Logger.Info("Completed");
        }

        void filters_EventColorControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<Product> lstProductColor = sender as List<Product>;
            ColorsUc color = new ColorsUc();
            color.lstProductsColorWise = lstProductColor;
            GridCarDetailsPanel.Children.Add(color);
            color.EventShowSelectedColorCar += color_EventShowSelectedColorCar;
            Logger.Info("Completed");
        }

        void color_EventShowSelectedColorCar(object sender, EventArgs e)
        {
            //load selected color car details
        }

        private void LoadAudioControl()
        {
            Logger.Info("Initiated");
            audioCtrl = new Audio();
            string audioFolder = ConfigurationManager.AppSettings["AudioPath"];
            string audiopath = System.IO.Path.Combine(Apps.strPath, audioFolder);
            if (File.Exists(audiopath))
            {
                GridAudioPanel.Visibility = Visibility.Visible;
                string strInfoAudio = System.IO.File.ReadAllText(audiopath);
                audioCtrl = Utilities.GetAudioData(strInfoAudio);
                if (audioCtrl != null)
                {                    
                    string path = audioCtrl.AudioItem.AudioLocation;
                    MEAudio.Source = new System.Uri(path);
                    MEAudio.Position = new TimeSpan(0, 0, 32);
                }
            }
            Logger.Info("Completed");
        }

        void filter_EventLoadCarMenu(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LoadHome();
            Logger.Info("Completed");
        }

        void filter_EventOpenCarImagesUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Product product = sender as Product;
            string carId = sender as string;
            CloseCarUC();
            CloseBrochureUC();
            CloseVariantsUC();
            CloseGalleryUC();
            CloseOverviewUC();
            CloseImageViewUC();
            LoadCarUc(carId);
            LoadOverview(product);
            Logger.Info("Completed");            
        }

        private void LoadOverview(Product product)
        {
            OverviewUc overView = new OverviewUc();
            overView.product = product;
            //overView.carId = carId;
            GridCarDetailsPanel.Children.Add(overView);
            overView.EventOpenOverviewItem += overView_EventOpenOverviewItem;
        }
        
        void overView_EventOpenOverviewItem(object sender, EventArgs arg)
        {
            if (isOverviewItemDisplayed)
            {
                Button btn = sender as Button;
                ovItem = btn.DataContext as OverviewItem;
                if (ovItem != null)
                {
                    timerImagesChange = new DispatcherTimer();
                    timerImagesChange.Interval = new TimeSpan(0, 0, 0, 0, 15);
                    timerImagesChange.Tick += new EventHandler(timerImagesChange_Tick);

                    foreach (UIElement item in GridCarDetailsPanel.Children)
                    {
                        if (item is CarUc)
                        {
                            car360 = item as CarUc;
                            destinationImageNumber = Int32.Parse(ovItem.Destination);
                            if (destinationImageNumber <= car360.imagesCount + 1)
                            {
                                string currentImagePath = car360.carImage.Source.ToString();
                                currentImageNumber = Int32.Parse(System.IO.Path.GetFileNameWithoutExtension(currentImagePath.Substring(currentImagePath.LastIndexOf('/') + 1)));
                                isOverviewItemDisplayed = false;
                                timerImagesChange.Start();
                            }
                        }
                    }
                }
            }
        }
        private void timerImagesChange_Tick(object sender, EventArgs e)
        {
            isOverviewItemDisplayed = false;
            if (destinationImageNumber < currentImageNumber)
            {
                for (; currentImageNumber >= destinationImageNumber; )
                {
                    string path = "pack://siteoforigin:,,,/Images/Car360/" + ovItem.CarID + "/" + currentImageNumber + ".png";
                    car360.carImage.Source = new BitmapImage(new Uri(path));
                    currentImageNumber--;
                    break;
                }
            }
            else if (destinationImageNumber > currentImageNumber)
            {
                for (; currentImageNumber <= destinationImageNumber; )
                {
                    string path = "pack://siteoforigin:,,,/Images/Car360/" + ovItem.CarID + "/" + currentImageNumber + ".png";
                    car360.carImage.Source = new BitmapImage(new Uri(path));
                    currentImageNumber++;
                    break;
                }
            }
            else if (destinationImageNumber == currentImageNumber)
            {
                string path = ovItem.TempLocation;
                car360.carImage.Source = new BitmapImage(new Uri(path));
                if (timerImagesChange != null)
                {
                    timerImagesChange.Stop();
                    timerImagesChange = null;
                    isOverviewItemDisplayed = true;
                }
            }
        }

        private void LoadCarUc(string carId)
        {
            CarImageItem carItem = carImagesData.CarImageItem.Where(c => c.CarID.Equals(carId)).FirstOrDefault();
            if (carItem != null)
            {
                CarUc carControl = new CarUc();
                carControl.carItem = carItem;
                GridCarDetailsPanel.Children.Add(carControl);
            }
        }

        #region BrochureControls
        void filter_EventOpenBroucherUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseGalleryUC();
            CloseCarUC();
            CloseVariantsUC();
            CloseImageViewUC();
            CloseOverviewUC();

            string carId = sender as string;
            BrochureUc broc = new BrochureUc();
            broc.carID = carId;
            broc.BringIntoView();
            Panel.SetZIndex(broc, SetZIndexValue());
            GridCarDetailsPanel.Children.Add(broc);
            broc.EventOpenBrochureItem += broc_EventOpenBrochureItem;
            Logger.Info("Completed");
        }

        void broc_EventOpenBrochureItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string path = sender as string;
            LoadImage(path);
            Logger.Info("Completed");
        }
        #endregion

        void filter_EventOpenVariantsUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseGalleryUC();
            CloseImageViewUC();
            CloseBrochureUC();
            CloseCarUC();
            CloseOverviewUC();

            //load carImages control
            string carId = sender as string;
            LoadCarUc(carId);

            //load variants
            VariantsUc variant = new VariantsUc();
            variant.selectedCarId = carId;
            GridCarDetailsPanel.Children.Add(variant);
            variant.EventOpenItemDetails += variant_EventOpenItemDetails;

            //load colors
            ColorsUc color = new ColorsUc();
            color.selectedCarId = carId;
            GridCarDetailsPanel.Children.Add(color);
            color.EventShowSelectedCar += color_EventShowSelectedCar;

            Logger.Info("Completed");
        }
        void color_EventShowSelectedCar(object sender, EventArgs e)
        {
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is CarUc)
                {
                    ColorItem clrItem = sender as ColorItem;
                    string imageLocation;
                    CarUc caruc;
                    switch (clrItem.Name)
                    {
                        case "Berry Red":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t-BerryRed.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Expresso Brown":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t-ExpressoBrown.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Striker Blue":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t-StrikerBlue.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Sunburst Orange":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t-SunburstOrange.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Platinum Silver":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t-PlatinumSilver.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Pearlescent White":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t-PearlescentWhite.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Astern Black":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Strome/s-AsternBlack.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Urban Bronze":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Strome/s-UrbanBronze.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Arctic White":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Strome/s-ArcticWhite.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;
                        case "Pearl White":
                            imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Strome/s-PearlWhite.png";
                            caruc = item as CarUc;
                            caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                            break;


                        default:
                            break;
                    }



                    //imageLocation = imageLocation.Insert(imageLocation.Length - 4, "1");
                    //if (imageLocation.Equals("pack://siteoforigin:,,,/Images/CarColor/Tiago/t21.png"))
                    //string imageLocation = "pack://siteoforigin:,,,/Images/CarColor/Tiago/t21.png";
                    //if(clrItem.Name.Equals("Berry Red"))
                    //{
                    //    CarUc caruc = item as CarUc;
                    //    caruc.carImage.Source = new BitmapImage(new Uri(imageLocation));
                    //    caruc.carImage.Stretch = Stretch.Uniform;
                    //}
                }
            }
        }
        void variant_EventOpenItemDetails(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            VariantItem vItems = sender as VariantItem;
            ClearVariantDetailsData();
            VariantDetailUc vDets = new VariantDetailUc();
            vDets.vItem = vItems;
            GridCarDetailsPanel.Children.Add(vDets);
            vDets.EventAddToCompare += vDets_EventAddToCompare;
            vDets.EventCloseCarDetails += vDets_EventCloseCarDetails;
            Logger.Info("Completed");
        }

        void vDets_EventCloseCarDetails(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            VariantDetailUc vDets = (VariantDetailUc)sender;
            GridCarDetailsPanel.Children.Remove(vDets);
            Logger.Info("Completed");            
        }

        void vDets_EventAddToCompare(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Button btn = sender as Button;
            VariantItem vItem = btn.DataContext as VariantItem;
            if (vItem != null)
            {
                if (!(Apps.lstSelectedVariant.Any(c => c.SNo.Equals(vItem.SNo))))
                {
                    Apps.lstSelectedVariant.Add(vItem);
                    btnCompare.Content = Apps.lstSelectedVariant.Count;
                    string msg = vItem.Name + " " + vItem.Version + "  successfully added to compare";
                    SuccessAlert(msg);
                }
                else
                {
                    string msg = vItem.Name + " " + vItem.Version + "  added to compare";
                    WarningAlert(msg);
                }
                Logger.Info("Completed");
            }
        }
        private void ClearVariantDetailsData()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is VariantDetailUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c); //remove
            });
        }
        
        void filter_EventOpenGalleryUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseBrochureUC();
            CloseVariantsUC();
            CloseCarUC();
            CloseOverviewUC();
            CloseImageViewUC();
            string carId = sender as string;
            List<GalleryItem> gItem = carGalleryData.GalleryItem.Where(c => c.CarID.Equals(carId)).ToList();
            if (gItem != null)
            {
                GalleryUc galleryControl = new GalleryUc();
                galleryControl.gItem = gItem;
                GridCarDetailsPanel.Children.Add(galleryControl);
                galleryControl.EventOpenExteriorImages += galleryControl_EventOpenExteriorImages;
                galleryControl.EventOpenInteriorImages += galleryControl_EventOpenInteriorImages;
                galleryControl.EventCloseImageControl += galleryControl_EventCloseImageControl;
            }
            Logger.Info("Completed");
        }
        void galleryControl_EventOpenExteriorImages(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<GalleryItem> lstGalleryItems = sender as List<GalleryItem>;
            foreach (GalleryItem item in lstGalleryItems)
            {
                int xPoint = Int32.Parse(item.XAxis);
                int yPoint = Int32.Parse(item.YAxis);
                LoadGalleryImage(item.ImageLocation, xPoint, yPoint);
            }
            Logger.Info("Completed");
        }
        void galleryControl_EventOpenInteriorImages(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<GalleryItem> lstGalleryItems = sender as List<GalleryItem>;
            foreach (GalleryItem item in lstGalleryItems)
            {
                int xPoint = Int32.Parse(item.XAxis);
                int yPoint = Int32.Parse(item.YAxis);
                LoadGalleryImage(item.ImageLocation, xPoint, yPoint);
            }
            Logger.Info("Completed");            
        }
        private void LoadGalleryImage(string item, int xPoint, int yPoint)
        {
            Logger.Info("Initiated");
            if (CheckItemExists(item))
            {                
                ImageViewUc image = new ImageViewUc();
                image.dataItemDetails = item;
                image.IsManipulationEnabled = true;
                image.RenderTransform = new MatrixTransform(1, 0, 0, 1, xPoint, yPoint);
                image.BringIntoView();
                Panel.SetZIndex(image, SetZIndexValue());
                GridPopUpPanel.Children.Add(image);
                image.EvntCloseImageViewUc += image_EvntCloseImageViewUc;
            }
            Logger.Info("Completed");
        }
        void galleryControl_EventCloseImageControl(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseImageViewUC();
            Logger.Info("Completed");
        }
        
        private void CloseCarControls()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is ColorsUc || item is VariantsUc || item is GalleryUc 
                    || item is ImageViewUc || item is CarDetailsUc || item is FilterUc
                        || item is BrochureUc || item is VariantDetailUc || item is CarUc || item is OverviewUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });

            List<int> lstPopUpIndexes = new List<int>();
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is ImageViewUc)
                {
                    lstPopUpIndexes.Add(GridPopUpPanel.Children.IndexOf(item));
                }
            }
            lstPopUpIndexes = lstPopUpIndexes.OrderByDescending(c => c).ToList();
            lstPopUpIndexes.ForEach(c =>
            {
                GridPopUpPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }
        //close car menu control
        private void CloseCarMenuUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarPanel.Children)
            {
                if (item is CarMenuUc)
                {
                    lstIndexes.Add(GridCarPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close Image Control
        private void CloseImageViewUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is ImageViewUc)
                {
                    lstIndexes.Add(GridPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridPopUpPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close Overview Control
        private void CloseOverviewUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is OverviewUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close Brochure Control
        private void CloseBrochureUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is BrochureUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close variants Control which includes VariantsUc, VariantDetailUc and ColorsUc
        private void CloseVariantsUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is VariantsUc || item is VariantDetailUc || item is ColorsUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close variants Control which includes VariantsUc, VariantDetailUc and ColorsUc
        private void CloseGalleryUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is GalleryUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }

        //close CarUC user control
        private void CloseCarUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridCarDetailsPanel.Children)
            {
                if (item is CarUc)
                {
                    lstIndexes.Add(GridCarDetailsPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridCarDetailsPanel.Children.RemoveAt(c);   //remove popups by its index values
            });
            Logger.Info("Completed");
        }
        private void btnHomeLogo_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            LoadSlideshow();
            Logger.Info("Completed");
        }

        #region ImageView
        private void LoadImage(string item)
        {
            Logger.Info("Initiated");
            if (CheckItemExists(item))
            {
                ivRenTrsfX = new Random().Next(350, 600);
                ivRenTrsfY = new Random().Next(0, 100);
                ImageViewUc image = new ImageViewUc();
                image.dataItemDetails = item;
                image.IsManipulationEnabled = true;
                image.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
                image.BringIntoView();
                Panel.SetZIndex(image, SetZIndexValue());
                GridPopUpPanel.Children.Add(image);
                image.EvntCloseImageViewUc += image_EvntCloseImageViewUc;
            }
            Logger.Info("Completed");
        }

        private bool CheckItemExists(string location)
        {
            foreach (UIElement item in GridPopUpPanel.Children)
            {
                if (item is ImageViewUc)
                {
                    ImageViewUc imgView = item as ImageViewUc;
                    if (imgView.dataItemDetails.Equals(location))
                    {
                        ivRenTrsfX = new Random().Next(350, 600);
                        ivRenTrsfY = new Random().Next(0, 100);
                        imgView.RenderTransform = new MatrixTransform(1, 0, 0, 1, ivRenTrsfX, ivRenTrsfY);
                        imgView.BringIntoView();
                        Panel.SetZIndex(imgView, SetZIndexValue());
                        return false;
                    }
                }
            }
            return true;
        }
        void image_EvntCloseImageViewUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ImageViewUc image = (ImageViewUc)sender;
            GridPopUpPanel.Children.Remove(image);
            Logger.Info("Completed");
        }
        #endregion

        #region FloatingButton

        private void btnFloatingButton_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            if (!IsFloatingBtnSelected)
            {
                IsFloatingBtnSelected = true;
                FloatingButtonsUc fButtons = new FloatingButtonsUc();
                fButtons.BringIntoView();
                Panel.SetZIndex(fButtons, SetZIndexValue());
                GridFloatingContentPanel.Children.Add(fButtons);
                fButtons.EventCloseBrdFloatingUc += fButtons_EventCloseBrdFloatingUc;
                fButtons.EventContactUc += fButtons_EventContactUc;
                fButtons.EventEnquiryUc += fButtons_EventEnquiryUc;
                fButtons.EventFinanceUc += fButtons_EventFinanceUc;
                fButtons.EventFeedbackUc += fButtons_EventFeedbackUc;
                fButtons.EventScheduleTestDriveUc += fButtons_EventScheduleTestDriveUc;
                Storyboard RotatePlusButton_SB = this.TryFindResource("RotatePlusButton_SB") as Storyboard;
                RotatePlusButton_SB.Begin();
            }
            else
            {
                CloseFloatingButtonUC();
            }
            Logger.Info("Completed");
        }

        void fButtons_EventFeedbackUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated"); 
            CloseFloatingButtonUC();
            Feedback feedback = new Feedback();
            feedback.BringIntoView();
            Panel.SetZIndex(feedback, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(feedback);
            feedback.EventCloseFeedback += feedback_EventCloseFeedback;
            feedback.EventFeedbackSubmit += feedback_EventFeedbackSubmit;
            Logger.Info("Completed");            
        }

        void feedback_EventFeedbackSubmit(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            SuccessAlert(msg);
            Logger.Info("Completed"); 
        }

        void feedback_EventCloseFeedback(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            Feedback feedback = (Feedback)sender;
            GridFloatingPopUpPanel.Children.Remove(feedback);
            Logger.Info("Completed");            
        }
        
        #region ScheduleTestDrive
        void fButtons_EventScheduleTestDriveUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            ScheduleTestDriveUc sDrive = new ScheduleTestDriveUc();
            sDrive.gridContext.DataContext = currentSelectedCar;
            sDrive.BringIntoView();
            Panel.SetZIndex(sDrive, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(sDrive);
            sDrive.EventCloseTestDriveUc += sDrive_EventCloseTestDriveUc;
            sDrive.EventSubmitTestDrive += sDrive_EventSubmitTestDrive;
            Logger.Info("Completed");
        }

        void sDrive_EventSubmitTestDrive(object sender, EventArgs e)
        {
            string msg = sender as string;
            SuccessAlert(msg);
        }

        void sDrive_EventCloseTestDriveUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is ScheduleTestDriveUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        } 
        #endregion

        #region Finance
        void fButtons_EventFinanceUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            FinanceUc finance = new FinanceUc();
            finance.gridContext.DataContext = currentSelectedCar;
            finance.BringIntoView();
            Panel.SetZIndex(finance, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(finance);
            finance.EventCloseFinanceUc += finance_EventCloseFinanceUc;
            finance.EventFinanceSuccessAlert += finance_EventFinanceSuccessAlert;
            Logger.Info("Completed");
        }

        void finance_EventFinanceSuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            SuccessAlert(msg);
            Logger.Info("Completed");
        }

        void finance_EventCloseFinanceUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is FinanceUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        } 
        #endregion

        #region EnquiryForm
        void fButtons_EventEnquiryUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            EnquiryUc enquiry = new EnquiryUc();
            enquiry.gridContext.DataContext = currentSelectedCar;
            enquiry.BringIntoView();
            Panel.SetZIndex(enquiry, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(enquiry);
            enquiry.EventCloseEnquiryUc += enquiry_EventCloseEnquiryUc;
            enquiry.EventEnquirySuccessAlert += enquiry_EventEnquirySuccessAlert;
            Logger.Info("Completed");
        }

        void enquiry_EventEnquirySuccessAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            SuccessAlert(msg);
            Logger.Info("Completed");
        }

        void enquiry_EventCloseEnquiryUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is EnquiryUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        } 
        #endregion

        #region ContactUs
        void fButtons_EventContactUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseFloatingButtonUC();
            ContactUc contact = new ContactUc();
            contact.BringIntoView();
            Panel.SetZIndex(contact, SetZIndexValue());
            GridFloatingPopUpPanel.Children.Add(contact);
            contact.EventCloseContactUc += contact_EventCloseContactUc;
            Logger.Info("Completed");
        }

        void contact_EventCloseContactUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingPopUpPanel.Children)
            {
                if (item is ContactUc)
                {
                    lstIndexes.Add(GridFloatingPopUpPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingPopUpPanel.Children.RemoveAt(c);
            });
            Logger.Info("Completed");
        } 
        #endregion

        #region AlertUC

        private void WarningAlert(string msg)
        {
            Logger.Info("Initiated");
            WarningAlertUC wAlert = new WarningAlertUC();
            wAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(wAlert);
            wAlert.EvntCloseWarningAlert += wAlert_EvntCloseWarningAlert;
            Logger.Info("Completed");
        }

        void wAlert_EvntCloseWarningAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            WarningAlertUC wAlert = (WarningAlertUC)sender;
            GridAlertPopUpPanel.Children.Remove(wAlert);
            Logger.Info("Completed");
        }

        private void ErrorAlert(string msg)
        {
            Logger.Info("Initiated");
            ErrorAlertUC eAlert = new ErrorAlertUC();
            eAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(eAlert);
            eAlert.EvntCloseErrorAlert += eAlert_EvntCloseErrorAlert;
            Logger.Info("Completed");
        }

        void eAlert_EvntCloseErrorAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            ErrorAlertUC eAlert = (ErrorAlertUC)sender;
            GridAlertPopUpPanel.Children.Remove(eAlert);
            Logger.Info("Completed");
        }

        private void SuccessAlert(string msg)
        {
            Logger.Info("Initiated");
            SuccessAlertUC sAlert = new SuccessAlertUC();
            sAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(sAlert);
            sAlert.EventCloseSuccessMessageAlert += sAlert_EventCloseSuccessMessageAlert;
            Logger.Info("Completed");
        }

        void sAlert_EventCloseSuccessMessageAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            SuccessAlertUC sAlert = (SuccessAlertUC)sender;
            GridAlertPopUpPanel.Children.Remove(sAlert);
            Logger.Info("Completed");            
        }               
        
        private void MessageAlert(string msg)
        {
            Logger.Info("Initiated");
            MessageUc mAlert = new MessageUc();
            mAlert.tblMessage.Text = msg;
            GridAlertPopUpPanel.Children.Add(mAlert);
            mAlert.EventCloseMessageAlert += mAlert_EventCloseMessageAlert;
            Logger.Info("Completed");
        }

        void mAlert_EventCloseMessageAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            MessageUc mAlert = (MessageUc)sender;
            GridAlertPopUpPanel.Children.Remove(mAlert);
            Logger.Info("Completed");            
        }
        #endregion
        void fButtons_EventCloseBrdFloatingUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");            
            CloseFloatingButtonUC();
            Logger.Info("Completed");
        }
        private void CloseFloatingButtonUC()
        {
            Logger.Info("Initiated");
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridFloatingContentPanel.Children)
            {
                if (item is FloatingButtonsUc)
                {
                    lstIndexes.Add(GridFloatingContentPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridFloatingContentPanel.Children.RemoveAt(c);   //remove FloatingUC by its index values
            });
            Storyboard RotateCrossButton_SB = this.TryFindResource("RotateCrossButton_SB") as Storyboard;
            RotateCrossButton_SB.Begin();
            IsFloatingBtnSelected = false;
            Logger.Info("Completed");
        } 
        #endregion

        private void btnCompare_TouchDown(object sender, TouchEventArgs e)
        {
            Logger.Info("Initiated");
            btnFloatingButton.Visibility = Visibility.Collapsed;
            CompareUc compare = new CompareUc();
            compare.carImagesData = carImagesData;
            GridComparePanel.Children.Add(compare);
            compare.EventCloseCompareUc += compare_EventCloseCompareUc;
            compare.EventHomeLoad += compare_EventHomeLoad;
            compare.EventOpenCmpGalleryItem += compare_EventOpenCmpGalleryItem;
            compare.EventOpenWarningMessageAlert += compare_EventOpenWarningMessageAlert;
            compare.EventCloseCompareGalleryImages += compare_EventCloseCompareGalleryImages;
            Logger.Info("Completed");
        }

        void compare_EventCloseCompareGalleryImages(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            CloseImageViewUC();
            Logger.Info("Completed");            
        }

        void compare_EventOpenWarningMessageAlert(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string msg = sender as string;
            WarningAlert(msg);
            Logger.Info("Completed");
        }
        
        void compare_EventHomeLoad(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            LoadSlideshow();
            Logger.Info("Completed");            
        }

        void compare_EventOpenCmpGalleryItem(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            string strLocation = sender as string;
            LoadImage(strLocation);
            Logger.Info("Completed");
        }
        void compare_EventCloseCompareUc(object sender, EventArgs e)
        {
            Logger.Info("Initiated");
            btnFloatingButton.Visibility = Visibility.Visible;
            CloseImageViewUC();
            if (Apps.lstSelectedVariant != null)
            {
                btnCompare.Content = Apps.lstSelectedVariant.Count;
            }
            else { btnCompare.Content = "0"; }

            CompareUc compare = (CompareUc)sender;
            GridComparePanel.Children.Remove(compare);
            Logger.Info("Completed");
        }

        private void btnAudioMute_TouchDown(object sender, TouchEventArgs e)
        {
            MEAudio.Play();
            btnAudioMute.Visibility = Visibility.Collapsed;
            btnAudioVolume.Visibility = Visibility.Visible;
        }

        private void btnAudioVolume_TouchDown(object sender, TouchEventArgs e)
        {
            MEAudio.Pause();
            btnAudioMute.Visibility = Visibility.Visible;
            btnAudioVolume.Visibility = Visibility.Collapsed;
        }

        private void MEAudio_MediaEnded(object sender, RoutedEventArgs e)
        {
            LoadAudioControl();
        }

        #region AdminLogin
        private void AdminLogin()
        {
            LoginUc login = new LoginUc();
            GridAdminPanel.Children.Add(login);
            login.EventAdminLogin += login_EventAdminLogin;
            login.EventCloseAdminLogin += login_EventCloseAdminLogin;
        }
        void login_EventAdminLogin(object sender, EventArgs e)
        {
            RemoveAdminLoginControl();
            LoadAdminDashboard();
        }
        private void RemoveAdminLoginControl()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridAdminPanel.Children)
            {
                if (item is LoginUc)
                {
                    lstIndexes.Add(GridAdminPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridAdminPanel.Children.RemoveAt(c); //remove
            });
        }
        void login_EventCloseAdminLogin(object sender, EventArgs e)
        {
            LoginUc login = (LoginUc)sender;
            GridAdminPanel.Children.Remove(login);
        } 
        #endregion
        #region AdminDashboard
        private void LoadAdminDashboard()
        {
            DashboardUc dashboard = new DashboardUc();
            GridAdminPanel.Children.Add(dashboard);
            dashboard.EventOpenSetAutoSync += dashboard_EventOpenSetAutoSync;
            dashboard.EventLogoutSettings += dashboard_EventLogoutSettings;
        }
        void dashboard_EventLogoutSettings(object sender, EventArgs e)
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridAdminPanel.Children)
            {
                if (item is LoginUc || item is AutoSyncUc ||  item is DashboardUc)
                {
                    lstIndexes.Add(GridAdminPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridAdminPanel.Children.RemoveAt(c); //remove
            });
        }
        private void RemoveAdminDashboard()
        {
            List<int> lstIndexes = new List<int>();
            foreach (UIElement item in GridAdminPanel.Children)
            {
                if (item is DashboardUc)
                {
                    lstIndexes.Add(GridAdminPanel.Children.IndexOf(item));
                }
            }
            lstIndexes = lstIndexes.OrderByDescending(c => c).ToList();
            lstIndexes.ForEach(c =>
            {
                GridAdminPanel.Children.RemoveAt(c); //remove
            });
        }
        #endregion
        #region AdminAutosync
        void dashboard_EventOpenSetAutoSync(object sender, EventArgs e)
        {
            RemoveAdminDashboard();
            AutoSyncUc autosync = new AutoSyncUc();
            GridAdminPanel.Children.Add(autosync);
            autosync.EventCloseSetAutoSync += autosync_EventCloseSetAutoSync;
            autosync.EventSuccessAlert += autosync_EventSuccessAlert;
            autosync.EventErrorAlert += autosync_EventErrorAlert;
        }

        void autosync_EventErrorAlert(object sender, EventArgs e)
        {
            string msg = sender as string;
            ErrorAlert(msg);
        }

        void autosync_EventSuccessAlert(object sender, EventArgs e)
        {
            string msg = sender as string;
            SuccessAlert(msg);
        }

        void autosync_EventCloseSetAutoSync(object sender, EventArgs e)
        {
            AutoSyncUc autosync = (AutoSyncUc)sender;
            GridAdminPanel.Children.Remove(autosync);
            LoadAdminDashboard();
        } 
        #endregion
    }
}
